<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>
 
## About API Endpoint Bali Tower News Endpoint build On Laravel

Haiiii, ini adalah API Endpoint dengan menggunakan laravel 9 
dan menggunakan package bawaan dari laravel itu sendiri seperti :

- Laravel Passport
- Laravel API Eloquent Resource
- Repository Patterd Design 


## How to deploy ?

- composer install
- duplicate and rename .env.example to .env
- php artisan key:gen && php artisan passport:install --force && php artisan db:seed
- enjoy your using API Endpoint  

## If you use a docker

Use the docker-compose to install application. 
```bash
docker compose up -d
```
after the docker-compose continue to access web for migrate & seeder data. 
```bash
https://localhost/call-migration
``` 

postman link public :https://elements.getpostman.com/redirect?entityId=4270905-04990006-0e90-4c89-88e6-ac30c9a8ac0f&entityType=collection
