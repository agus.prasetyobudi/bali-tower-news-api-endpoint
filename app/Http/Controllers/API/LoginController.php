<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Auth\LoginRequest;
use App\Http\Resources\Auth\LoginResource;
use App\Interfaces\IUserServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    public $services;
    public function __construct(IUserServices $services)
    {
        $this->services = $services;
    }
    public function login(LoginRequest $request)
    {
        try {
            if($auth = $this->services->login($request)){
                return response()->json(new LoginResource($auth));
            }
            return ResponseHelpers::errorResponse(400,"User Not Authenticated, Email / Password Wrong");
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return ResponseHelpers::errorResponse(400,"Authentication Error, Call Admin");

        }
    }

    public function Logout(Request $request)
    {
        try {
            if($this->services->Logout($request)){
                return ResponseHelpers::successResponse(200,"User Has Logged Out");
            }
                return ResponseHelpers::errorResponse(400,'Authentication error, Call Admin');
            } catch (\Throwable $th) {
                Log::error($th->getMessage());
                return ResponseHelpers::errorResponse(400,'Authentication error, Call Admin');
        }
    }
}
