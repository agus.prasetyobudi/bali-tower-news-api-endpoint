<?php

namespace App\Http\Resources\API\Comment;

use App\Http\Resources\API\News\Author\AuthorResource;
use App\Http\Resources\API\News\NewsSlugResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class GetCommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'newsId'=> $this->news_id,
            'commented' => [
                'name' => $this->users->name,
                'email' => $this->users->email??'',
                'joined_at' => Carbon::parse($this->users->created_at)->format('Y-m-d H:i:s'),
            ],
            'comment' => $this->comment,
            'has_edited' => $this->has_update ==1? true:false,
            'comment_at' =>  $this->has_update ==1? $this->updated_at:$this->created_at,
        ];
    }
}
