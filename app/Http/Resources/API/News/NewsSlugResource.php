<?php

namespace App\Http\Resources\API\News;

use App\Http\Resources\API\Comment\GetCommentResource;
use App\Http\Resources\API\News\Author\AuthorResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class NewsSlugResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'images' => Storage::url($this->images),
            'author' => new AuthorResource($this->author),
            'post' => $this->post,
            'comment' => $this->comments
        ];
    }
}
