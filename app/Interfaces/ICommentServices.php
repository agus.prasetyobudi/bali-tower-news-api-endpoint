<?php

namespace App\Interfaces;

use Illuminate\Http\Request;

interface ICommentServices
{
    public function GetComment(Request $request);
    public function StoreComment(Request $request);
    public function UpdateComment(Request $request);
    public function getCommentFromNews($id);
}
