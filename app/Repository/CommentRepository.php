<?php

namespace App\Repository;

use App\Http\Resources\API\Comment\GetCommentResource;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentRepository extends BaseRepository
{
    public function __construct(Comment $model)
    {
        $this->model = $model;
    }
    public function findByUser($id,$page = 10)
    {
        if($cache = $this->redis_get("comment:userId:$id")){
            return $cache;
        }
        $paginate= $this->model->where('user_id',$id)->paginate($page);
        $paginate->setPath(env('PAGINATION_URL')."/comments/get?user=$id");
        return $paginate;
    }
    public function findByNews($id,$page = 10)
    {
        if($cache = $this->redis_get("comment:newsPaginteId:$id")){
            return $cache;
        }
        $paginate= $this->model->where('news_id',$id)->paginate($page);
        $paginate->setPath(env('PAGINATION_URL')."/comments/get?news=$id");
        return $paginate;
    }
    public function findByNewsServices($id)
    {
        if($cache = $this->redis_get("news:commentNews:$id")){
            return $cache;
        }
        $model= $this->model->where('news_id',$id)->get();
        $model->cache = GetCommentResource::collection($model);
        $this->redis_store("news:commentNews:$id",$model->cache,120);
        return $model->cache;
    }
    public function create(Request $request)
    {
        $model = $this->model->create($this->payloads($request));
        $this->redis_store("comment:newsId:$model->news_id",$this->model->where('news_id',$request->postId)->get());
        return $model;
    }

    public function updates(Request $request)
    {
        $this->model->where('id',$request->comment_id)->update($this->payloads($request));
        $model = $this->model->find($request->comment_id);
        $paginate= $this->model->where('news_id',$model->news_id)->paginate(10);
        $paginate->setPath(env('PAGINATION_URL')."/comments/get?news=$model->news_id");
        $this->redis_store('comment:newsPaginteId:'.$model->news_id,$paginate);
        return $model;

    }

    public function payloads(Request $request)
    {
        return[
            'user_id'=>auth()->user()->id,
            'comment' => $request->comment,
            'news_id' => $request->postId,
            'has_update' => $request->has_update??0
        ];
    }

    // private function commentFromNewsRedis ($data){
    //     return [

    //     ];
    // }
}
