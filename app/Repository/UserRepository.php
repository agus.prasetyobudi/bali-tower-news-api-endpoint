<?php

namespace App\Repository;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UserRepository extends BaseRepository
{
    public $model;
    public function __construct(User $model)
    {
        $this->model = $model;
    }
    public function findUser($id) :Collection
    {
        return $this->model->findOrFail($id);
    }
}
