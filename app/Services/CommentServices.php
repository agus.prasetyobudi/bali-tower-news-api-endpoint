<?php

namespace App\Services;

use App\Helpers\ResponseHelpers;
use App\Http\Resources\API\Comment\GetCommentResource;
use App\Http\Resources\CommentPaginateResource;
use App\Interfaces\ICommentServices;
use App\Repository\CommentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CommentServices implements ICommentServices
{
    private $model;
    public function __construct(CommentRepository $model)
    {
        $this->model = $model;
    }
    public function GetComment(Request $request)
    {
        try {
            if($request->query('user')||$request->query('page')){
                $model = $this->model->findByUser($request->query('user'),$request->query('page'));
                return ResponseHelpers::successResponse(200,'List Comment By User',$model);
            }
            if($request->query('news')||$request->query('page')){
                $model = $this->model->findByNews($request->query('news'),$request->query('page'));
                return ResponseHelpers::successResponse(200,'List Comment By News',$model);
            }
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return ResponseHelpers::errorResponse(400,"Commented Error");
        }
    }
    public function StoreComment(Request $request)
    {
       try {
        $model = $this->model->create($request);
        return ResponseHelpers::successResponse(200,"Comment Has Added",new GetCommentResource($model));
       } catch (\Throwable $th) {
        throw $th;
       }
    }
    public function UpdateComment(Request $request)
    {
        try {
            $request->merge(['has_update'=>1]);
            $model = $this->model->updates($request);
            return ResponseHelpers::successResponse(200,"Comment Has Update",new GetCommentResource($model));
           } catch (\Throwable $th) {
            throw $th;
           }
    }

    public function getCommentFromNews($id)
    {
        $model = $this->model->findByNewsServices($id);
        return $model;
    }
}
